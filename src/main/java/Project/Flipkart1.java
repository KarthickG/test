package Project;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class Flipkart1 {
@Test
	public void clickFirstMiMobile() {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();

		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://flipkart.com");
		driver.getKeyboard().sendKeys(Keys.ESCAPE);
		Actions builder = new Actions(driver);
		WebElement Electronics = driver.findElementByXPath("//span[text()='Electronics']");
		builder.moveToElement(Electronics).perform();

		driver.findElementByXPath("//a[text()='Mi']").click();

		WebDriverWait wait= new WebDriverWait(driver,20);
		wait.until(ExpectedConditions.titleContains("Mi"));
		String title = driver.getTitle();
		System.out.println(title);
		if (title.contains("Mi")) 
			System.out.println("Title contins Mi ");
		else 
			System.out.println("Title doesn't contains Mi");
		driver.findElementByXPath("//div[text()='Newest First']").click();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		List<WebElement> brand = driver.findElementsByXPath("//div[@class='col col-7-12']/div[1]");
		List<String> brandName= new ArrayList<>();

		for(WebElement i: brand)
		{
			brandName.add(i.getText());
		}
		List<WebElement> price = driver.findElementsByXPath("//div[@class='_1vC4OE _2rQ-NK']");
		
		List<String> priceValue = new ArrayList<>();
		for(WebElement i: price)
		{
			String k= (i.getText()).replaceAll("[\\D]","");
			priceValue.add(k);
			
		}
		int j=0;
		for(String i : brandName)
		{
			System.out.println("Brand "+(j+1)+" is "+i+" Price is "+priceValue.get(j));
			j++;
		}
		
		brand.get(0).click();
		/*Set<String> wins = driver.getWindowHandles();
		List<String> windows= new ArrayList<>();
		windows.addAll(wins);
		driver.switchTo().window(windows[1]);*/
	}






}


